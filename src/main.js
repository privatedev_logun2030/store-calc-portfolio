import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import lodash from 'lodash'
import VueLodash from 'vue-lodash'
import './main.styl'
Vue.use(VueLodash, lodash)
Vue.use(Vuetify)
new Vue({
	el: '#app',
	render: h => h(App)
})
